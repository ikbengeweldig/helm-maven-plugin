---
apiVersion: v2
name: ${NAME}
description: ${DESCRIPTION}
type: application
version: ${VERSION}
appVersion: ${APP_VERSION}
maintainers:
  - name: Khodabakhsh Bakhtiari
    email: khodabakhsh.ba@gmail.com
