package com.kbakhtiari.helm.maven.plugin;

import lombok.Data;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import static org.apache.commons.lang3.StringUtils.EMPTY;

@Data
@Mojo(name = "lint", defaultPhase = LifecyclePhase.TEST)
public class LintMojo extends AbstractHelmMojo {

  private static final String LINT_ARGS_TEMPLATE = "%s %s";

  @Parameter(property = "helm.lint.skip", defaultValue = "false")
  private boolean skipLint;

  @Parameter(property = "helm.lint.strict", defaultValue = "false")
  private boolean lintStrict;

  public void execute() throws MojoExecutionException {

    if (skip || skipLint) {
      getLog().info("skip linting");
      return;
    }
    for (String inputDirectory : getChartDirectories(getChartDirectory())) {

      getLog().info("Testing chart " + inputDirectory);
      callCli(
          getHelmCommand(
              "lint",
              String.format(LINT_ARGS_TEMPLATE, lintStrict ? "--strict" : EMPTY, inputDirectory)),
          "There are test failures");
    }
  }
}
