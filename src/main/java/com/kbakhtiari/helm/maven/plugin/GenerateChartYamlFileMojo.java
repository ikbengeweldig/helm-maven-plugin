package com.kbakhtiari.helm.maven.plugin;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.common.collect.ImmutableMap;
import lombok.Data;
import lombok.SneakyThrows;
import org.apache.maven.model.Contributor;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@Mojo(name = "generate-chart-yaml", defaultPhase = LifecyclePhase.INITIALIZE)
public class GenerateChartYamlFileMojo extends AbstractHelmMojo {

  @Parameter(property = "helm.generate-chart-yaml.skip", defaultValue = "true")
  private boolean skipGenerateChartYaml;

  @SneakyThrows
  public void execute() {

    if (skip || skipGenerateChartYaml) {
      getLog().info("skip generating chart yaml file");
      return;
    }

    getLog().debug("making empty yaml file in: " + getChartDirectory());
    Paths.get(getChartDirectory()).resolve("Chart.yaml").toFile().createNewFile();

    for (String inputDirectory : getChartDirectories(getChartDirectory())) {

      getLog().info("generating the helm Chart.yaml at " + inputDirectory);

      final List<ImmutableMap<String, String>> maintainers =
          ((List<Contributor>) getProject().getDevelopers())
              .stream()
                  .map(
                      contributor ->
                          ImmutableMap.<String, String>builder()
                              .put("name", contributor.getName())
                              .put("email", contributor.getEmail())
                              .build())
                  .collect(Collectors.toList());

      final Map chartProps =
          ImmutableMap.builder()
              .put("apiVersion", "v2")
              .put("name", getProject().getArtifactId())
              .put("description", getProject().getDescription())
              .put("type", "application")
              .put("version", getProject().getVersion())
              .put("appVersion", getProject().getVersion())
              .put("maintainers", maintainers)
              .build();

      final Path path =
          Paths.get(
              new StringBuilder(inputDirectory)
                  .append(File.separator)
                  .append("Chart.yaml")
                  .toString());

      if (path.toFile().exists()) {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.writeValue(path.toFile(), chartProps);
      }
    }
  }
}
