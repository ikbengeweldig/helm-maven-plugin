package com.kbakhtiari.helm.maven.plugin;

import lombok.Data;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Data
@Mojo(name = "template", defaultPhase = LifecyclePhase.VERIFY)
public class TemplateMojo extends AbstractHelmMojo {

  @Parameter(property = "helm.template.skip", defaultValue = "false")
  private boolean skipTemplate;

  public void execute() throws MojoExecutionException, MojoFailureException {

    if (skip || skipTemplate) {
      getLog().info("skip templating");
      return;
    }

    for (String inputDirectory : getChartDirectories(getChartDirectory())) {

      getLog().info("templating the chart " + inputDirectory);

      callCli(getCommand("template", inputDirectory), "There are test failures");
    }
  }
}
