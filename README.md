# What is helm maven plugin?

This is a Maven plugin for testing, packaging and uploading HELM charts.

"HELM is a tool for managing Kubernetes charts. Charts are packages of pre-configured Kubernetes resources."

Visit https://docs.helm.sh for detailed information.

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.kbakhtiari/helm-maven-plugin/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.kbakhtiari/helm-maven-plugin)

[comment]: <> ([![Build Status]&#40;https://gitlab.com/ikbengeweldig/helm-maven-plugin/badges/develop/pipeline.svg&#41;]&#40;https://gitlab.com/ikbengeweldig/helm-maven-plugin/-/commits/develop&#41;)

# Why?

Currently, (December 2020) there is no simple Maven plugin handling complex configurations to package existing HELM
charts. This project was initially forked from [kiwigrid](https://github.com/kiwigrid/helm-maven-plugin) brilliant
effort. Especially, this plugin is capable of integrate very well with GitLab CI/CD and NGINX ingress kubernetes annotations.

# How?

By default, the plugin automatically downloads Helm at the specified version. You can also manually specify the download
URL. Next to that it is possible to specify a local Helm binary. In all cases Helm will be executed in the background.

Add following plugin snippet to your pom.xml:

```xml
<plugin>
    <groupId>com.kbakhtiari</groupId>
    <artifactId>helm-maven-plugin</artifactId>
    <version>0.0.2</version>
</plugin>
```

## Configuration Examples

### Helm URL Auto Detection

The default setting is to construct the Helm download URL based upon the detected OS and architecture:

```xml
<build>
    <plugins>
        <plugin>
            <groupId>com.kbakhtiari</groupId>
            <artifactId>helm-maven-plugin</artifactId>
            <version>0.0.2</version>
            <configuration>
                <chartDirectory>${project.basedir}/helm/</chartDirectory>
                <chartVersion>${project.version}</chartVersion>
                <helmVersion>3.4.2</helmVersion>
            </configuration>
        </plugin>
    </plugins>
</build>
```

### Usage with Downloaded Binary

```xml
<build>
    <plugins>
        ...
        <plugin>
            <groupId>com.kbakhtiari</groupId>
            <artifactId>helm-maven-plugin</artifactId>
            <version>0.0.2</version>
            <configuration>
                <chartDirectory>${project.basedir}/helm/</chartDirectory>
                <chartVersion>${project.version}</chartVersion>
                <!-- This is the related section when using binary download -->
                <helmDownloadUrl>https://get.helm.sh/helm-v3.4.2-linux-amd64.tar.gz</helmDownloadUrl>
            </configuration>
        </plugin>
        ...
    </plugins>
</build>
```

### Usage with Local Binary

When `useLocalHelmBinary` is enabled, the plugin by default will search for the `helm` executable in `PATH`:

```xml
<build>
    <plugins>
        ...
        <plugin>
            <groupId>com.kbakhtiari</groupId>
            <artifactId>helm-maven-plugin</artifactId>
            <version>0.0.2</version>
            <configuration>
                <chartDirectory>${project.basedir}/helm/</chartDirectory>
                <chartVersion>${project.version}</chartVersion>
                <!-- This is the related section to use local binary with auto-detection enabled. -->
                <useLocalHelmBinary>true</useLocalHelmBinary>
            </configuration>
        </plugin>
        ...
    </plugins>
</build>
```

The following is an example configuration that explicitly sets the directory in which to look for the `helm` executable,
and disables the auto-detection feature:

```xml
<build>
    <plugins>
        ...
        <plugin>
            <groupId>com.kbakhtiari</groupId>
            <artifactId>helm-maven-plugin</artifactId>
            <version>0.0.2</version>
            <configuration>
                <chartDirectory>${project.basedir}/helm/</chartDirectory>
                <chartVersion>${project.version}</chartVersion>
                <!-- This is the related section to use local binary with auto-detection disabled. -->
                <useLocalHelmBinary>true</useLocalHelmBinary>
                <autoDetectLocalHelmBinary>false</autoDetectLocalHelmBinary>
                <helmExecutableDirectory>/usr/local/bin</helmExecutableDirectory>
            </configuration>
        </plugin>
        ...
    </plugins>
</build>
```

### More Complex Example

```xml
<build>
    <plugins>
        ...
        <plugin>
            <groupId>com.kbakhtiari</groupId>
            <artifactId>helm-maven-plugin</artifactId>
            <version>0.0.2</version>
            <configuration>
                <chartDirectory>${project.basedir}/helm/</chartDirectory>
                <chartVersion>${project.version}</chartVersion>
                <uploadRepoStable>
                    <name>stable-repo</name>
                    <url>https://registry.gitlab.example.com/project/helm-stable</url>
                    <username>foo</username>
                    <password>bar</password>
                </uploadRepoStable>
                <uploadRepoSnapshot>
                    <name>snapshot-repo</name>
                    <url>https://registry.gitlab.example.com/project/helm-snapshot</url>
                </uploadRepoSnapshot>
                <helmDownloadUrl>https://get.helm.sh/helm-v3.4.2-linux-amd64.tar.gz</helmDownloadUrl>
                <registryConfig>~/.config/helm/registry.json</registryConfig>
                <repositoryCache>~/.cache/helm/repository</repositoryCache>
                <repositoryConfig>~/.config/helm/repositories.yaml</repositoryConfig>
                <!-- Lint with strict mode -->
                <lintStrict>true</lintStrict>
                <!-- Disable adding of default repo stable https://charts.helm.sh/stable -->
                <addDefaultRepo>false</addDefaultRepo>
                <!-- Exclude a directory to avoid processing -->
                <excludes>
                    <exclude>${project.basedir}/excluded</exclude>
                    <exclude>${project.basedir}/**/excluded*</exclude>
                </excludes>
                <!-- Add an additional repo -->
                <helmExtraRepos>
                    <helmRepo>
                        <name>kbakhtiari</name>
                        <url>https://gitlab.example.com/ikbengeweldig/helm-maven-plugin</url>
                    </helmRepo>
                </helmExtraRepos>
                <!-- extra value settings for the lint command -->
                <values>
                    <overrides>
                        {
                            "component1":{
                                "install": {
                                    "path": "/opt/component1"
                                }
                            }
                        }
                    </overrides>
                    <yamlFile>${project.basedir}/src/test/resources/myOverrides.yaml</yamlFile>
                </values>
            </configuration>
        </plugin>
        ...
    </plugins>
</build>
```

### Complex example with overriding the values

One could use the following configuration along with GitLab CI:

```xml
<plugin>
    <groupId>com.kbakhtiari</groupId>
    <artifactId>helm-maven-plugin</artifactId>
    <version>0.0.2</version>
    <configuration>
        <releaseName>${project.artifactId}</releaseName>
        <chartVersion>${project.version}</chartVersion>
        <helmVersion>3.4.2</helmVersion>
        <namespace>${env.CI_NAMESPACE}</namespace>
        <chartDirectory>${project.basedir}/helm/</chartDirectory>
        <uploadRepoStable>
            <name>gitlab</name>
            <url>${env.CI_REGISTRY}/${env.CI_PROJECT_NAMESPACE}</url>
            <username>${env.CI_REGISTRY_USER}</username>
            <password>${env.CI_REGISTRY_PASSWORD}</password>
        </uploadRepoStable>
        <uploadRepoSnapshot>
            <name>gitlab</name>
            <url>${env.CI_REGISTRY}/${env.CI_PROJECT_NAMESPACE}</url>
            <username>${env.CI_REGISTRY_USER}</username>
            <password>${env.CI_REGISTRY_PASSWORD}</password>
        </uploadRepoSnapshot>
        <values>
            <overrides>
                <!-- @formatter:off -->
                {
                    "namespace": "${env.CI_NAMESPACE}",
                    "image": {
                        "tag": "${env.CI_NAMESPACE}-${env.CI_COMMIT_SHORT_SHA}",
                        "repository": "${env.CI_REGISTRY_IMAGE}"
                    },
                    "ingress": {
                        "enabled": "true",
                        "annotations": {
                            "^external-dns.alpha.kubernetes.io/target^": "${env.INGRESS_DOMAIN}",
                            "^nginx.ingress.kubernetes.io/rewrite-target^": "/$2"
                        },
                        "hosts": [
                            {
                                "host": "${env.INGRESS_DOMAIN}",
                                "paths": [
                                    "/${project.artifactId}(/|$)(.*)"
                                ]
                            }
                        ]
                    },
                    "imageCredentials": {
                        "name": "${project.artifactId}-container-registry",
                        "registry": "${env.CI_REGISTRY}",
                        "username": "${env.CI_DEPLOY_USER}",
                        "password": "${env.CI_DEPLOY_PASSWORD}",
                        "email": "${env.GITLAB_USER_EMAIL}"
                    }
                }
                <!-- @formatter:on -->
            </overrides>
        </values>
    </configuration>
</plugin>
```

and in the `.gitlab-ci.yaml` the following job:

```yaml
deploy-chart-dev:
  stage: deploy
  script:
    - export CI_NAMESPACE=dev
    - export INGRESS_DOMAIN=kbakhtiari.com
    - export HELM_EXPERIMENTAL_OCI=1
    - mvn helm:init helm:push
```

Having this job, you'll have a chart package in your registry with `values.yaml` file containing all the environment variables replaced. 

### overriding values with special case
As in the example above, if you want to have a value overridden with complex structure(like `external-dns.alpha.kubernetes.io/target` as the key which has forward slash in it ) you need to scape the key with wrapping it in caret character.( in this case `^external-dns.alpha.kubernetes.io/target^` ) 

# Features

- Package Helm charts from standard folder structure
- Test Helm charts (Helm lint)
- Recursive chart detection (subcharts)
- Helm does not need to be installed
- Upload to chart registries

# Usage

## Goals

- `helm:init` initializes Helm by downloading a specific version
- `helm:dependency-build` resolves the chart dependencies
- `helm:package` packages the given charts (chart.tar.gz)
- `helm:lint` tests the given charts
- `helm:dry-run` simulates an install
- `helm:push` pushes the packaged chart to the registry
- `helm:template` locally render templates
- `helm:upgrade` upgrade a release

## Configuration

Parameter | Type | User Property | Required | Description
--- | --- | --- | --- | ---
`<chartDirectory>` | string | helm.chartDirectory | true | root directory of your charts
`<chartVersion>` | string | helm.chartVersion | true | Version of the charts. The version have to be in the [SEMVER-Format](https://semver.org/), required by helm.
`<appVersion>` | string | helm.appVersion | false | The version of the app. This needn't be SemVer.
`<helmDownloadUrl>` | string | helm.downloadUrl | false | URL to download helm. Leave empty to autodetect URL based upon OS and architecture.
`<helmVersion>` | string | helm.version | false | Version of helm to download. Defaults to 3.4.2
`<excludes>` | list of strings | helm.excludes | false | list of chart directories to exclude
`<useLocalHelmBinary>` | boolean | helm.useLocalHelmBinary | false | Controls whether a local binary should be used instead of downloading it. If set to `true` path has to be set with property `executableDirectory`
`<autoDetectLocalHelmBinary>` | boolean | helm.autoDetectLocalHelmBinary | true | Controls whether the local binary should be auto-detected from `PATH` environment variable. If set to `false` the binary in `<helmExecutableDirectory>` is used. This property has no effect unless `<useLocalHelmBinary>` is set to `true`.
`<helmExecutableDirectory>` | string | helm.executableDirectory | false | directory of your helm installation (default: `${project.build.directory}/helm`)
`<outputDirectory>` | string | helm.outputDirectory | false | chart output directory (default: `${project.build.directory}/helm/repo`)
`<registryConfig>` | string | helm.registryConfig | false | path to the registry config file
`<repositoryCache>` | string | helm.repositoryCache | false | path to the file containing cached repository indexes
`<repositoryConfig>` | string | helm.repositoryConfig | false | path to the file containing repository names and URLs
`<helmExtraRepos>` | list of [HelmRepository](./src/main/java/com/kbakhtiari/helm/maven/plugin/pojo/HelmRepository.java) | helm.extraRepos | false | adds extra repositories while init
`<uploadRepoStable>`| [HelmRepository](./src/main/java/com/kbakhtiari/helm/maven/plugin/pojo/HelmRepository.java) | helm.uploadRepo.stable | true | Upload repository for stable charts
`<uploadRepoSnapshot>`| [HelmRepository](./src/main/java/com/kbakhtiari/helm/maven/plugin/pojo/HelmRepository.java) | helm.uploadRepo.snapshot | false | Upload repository for snapshot charts (determined by version postfix 'SNAPSHOT')
`<lintStrict>` | boolean | helm.lint.strict | false | run lint command with strict option (fail on lint warnings)
`<addDefaultRepo>` | boolean | helm.init.add-default-repo | true | If true, stable repo (https://charts.helm.sh/stable) will be added
`<skip>` | boolean | helm.skip | false | skip plugin execution
`<skipInit>` | boolean | helm.init.skip | false | skip init goal
`<skipLint>` | boolean | helm.lint.skip | false | skip lint goal
`<skipDryRun>` | boolean | helm.dry-run.skip | false | skip dry-run goal
`<skipDependencyBuild>` | boolean | helm.dependency-build.skip | false | skip dependency-build goal
`<skipPackage>` | boolean | helm.package.skip | false | skip package goal
`<skipUpload>` | boolean | helm.upload.skip | false | skip upload goal
`<values>` | [ValueOverride](./src/main/java/com/kbakhtiari/helm/maven/plugin/pojo/ValueOverride.java) | helm.values | false | override some values for linting with helm.values.overrides (--set option), helm.values.stringOverrides (--set-string option), helm.values.fileOverrides (--set-file option) and last but not least helm.values.yamlFile (--values option)

## Packaging with the Helm Lifecycle

To keep your pom files small you can use 'helm' packaging. This binds `helm:init` to the initialize phase, `helm:lint`
to the test phase, `helm:package` to the package phase and `helm:upload` to the deploy phase.

```xml

<pom>
    <artifactId>my-helm-charts</artifactId>
    <version>0.0.2</version>
    <packaging>helm</packaging>
    ...
    <build>
        <plugins>
            <plugin>
                <groupId>com.kbakhtiari</groupId>
                <artifactId>helm-maven-plugin</artifactId>
                <!-- Mandatory when you use a custom lifecycle -->
                <extensions>true</extensions>
                <configuration>
                    ...
                </configuration>
            </plugin>
        </plugins>
        ....
    </build>
</pom>
```
